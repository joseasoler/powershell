﻿# Paths used by the profile are defined once here for convenience.
$env:DEV_HOME = "/dev"
$env:PROFILE_HOME = "${HOME}/Documents/PowerShell"

# Environment variables.
$env:Path = "$env:DEV_HOME\bin;" + $env:Path

# Starship prompt configuration.
$env:STARSHIP_CONFIG = "${env:PROFILE_HOME}/starship/starship.toml"
Invoke-Expression (&starship init powershell)

# Make tab completion work more like in bash.
Set-PSReadlineKeyHandler -Key Tab -Function MenuComplete

# Aliases providing bash-like behaviour.
New-Alias which get-command
New-Alias grep rg
Set-Alias cat bat

# Development aliases.
function dev { set-location "${env:DEV_HOME}" }
function profile { set-location "${env:PROFILE_HOME}" }
