# Powershell development environment

Windows development environment built around a [PowerShell profile](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_profiles).


## Setup

To get a working development environment and satisfy all dependencies, the following software must be installed.

### PowerShell

Get the latest version from https://github.com/PowerShell/PowerShell/

The version included in Windows 10 by default will not work as it uses a different path for storing its profile.

### Chocolatey

Most other applications required for having this development environment set up can be installed using Chocolatey. Install it and then (on an admin PowerShell):

```
choco install git microsoft-windows-terminal cascadiacodepl starship nano bat fd ripgrep
# The following packages are not required for using the profile.
choco install keepassxc notepadplusplus audacious
```

### Powershell profile

Using git, check out this repository in $Home\[My ]Documents\PowerShell.


## Configuration

### Git

Run these commands on PowerShell. Do not forget to replace user.name and user.email with your name and email respectively!

```
git config --global user.name 'Your Name And Surname'
git config --global user.email 'user.name@gmail.com'
git config --global pull.rebase true
git config --global rebase.autoStash true
git config --global merge.ff false
git config --global core.longpaths true
git config --global core.excludesfile "${HOME}/Documents/PowerShell/git/.gitignore_global"
git config --global core.commentChar '>'
git config --global core.editor 'nano'
git config --global color.ui true
```

### Windows Terminal

Set the profile's font to Cascadia Core PL. Set the starting path to `C:\dev`.
